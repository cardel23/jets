angular.module('starter.destinos',[])

.value('PARSE_CREDENTIALS',{
	APP_ID: 'KerHLc4m0puEnR7su5AY60o1BK0fbdom4ASZtKIR',
    REST_API_KEY:'r1asVx1DmbYLlRvSEQytHhR71vfb8Jsgse6KyAze'
})

.factory('LocacionService', ['$http', 'PARSE_CREDENTIALS', function($http, PARSE_CREDENTIALS){
	var destinos;
    var salida;
	var destino;

	return {
        getAll:function(){
            return $http.get('https://api.parse.com/1/classes/Destino',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
        },
        get:function(id){
            return $http.get('https://api.parse.com/1/classes/Destino/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
        },
        create:function(data){
            return $http.post('https://api.parse.com/1/classes/Destino',data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        edit:function(id,data){
            return $http.put('https://api.parse.com/1/classes/Destino/'+id,data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        delete:function(id){
            return $http.delete('https://api.parse.com/1/classes/Destino/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        setSalida:function(data){
            salida = data;
            // console.log(angular.toJson(salida));
        },
        getSalida:function(){
            // console.log("La salida es: "+angular.toJson(salida));
            return salida;
        },
        setDestino:function(data){
            destino = data;
            // console.log(angular.toJson(destino));
        },
        getDestino:function(){
            // console.log("El destino es: "+angular.toJson(destino));
            return destino;
        }
    }
}]);