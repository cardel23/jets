angular.module('starter.usuario',[])

.value('PARSE_CREDENTIALS',{
    APP_ID: 'KerHLc4m0puEnR7su5AY60o1BK0fbdom4ASZtKIR',
    REST_API_KEY:'r1asVx1DmbYLlRvSEQytHhR71vfb8Jsgse6KyAze'
})

.factory('UsuarioService', ['$http','PARSE_CREDENTIALS',function($http,PARSE_CREDENTIALS){
    var users;
    var user = {};
    var isLogin = false;
    var userPut = {};

    //Para almacenar estado, guardarlo en el factory y obtenerlo desde el controlador
    return {
        getUserPut:function(){
            console.log(angular.toJson(userPut));
            return userPut;
        },
        isSet:function(){
            return isLogin;
        },
        getAll:function(){
            users = $http.get('https://api.parse.com/1/classes/_User',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
            return users;
        },
        get:function(id){
            console.log("Login ", isLogin);
            if(!isLogin || user === undefined){
                console.log("Usuario indefinido");
                user = $http.get('https://api.parse.com/1/classes/_User/'+id,{
                    headers:{
                        'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                        'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    }
                });
                isLogin = true;
                userPut.id = id;
            }
            else{
                console.log("Usuario definido");
            }
            return user;
        },
        getUser:function(){
            console.log("Login ", isLogin);
            console.log("Usuario", angular.toJson(user));
            return user;
        },
        create:function(data){
            return $http.post('https://api.parse.com/1/classes/_User',data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        edit:function(id,data){
            
            userPut = $http.put('https://api.parse.com/1/classes/_User/'+id,data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
            console.log(angular.toJson(userPut));
            return userPut;

        },
        delete:function(id){
            return $http.delete('https://api.parse.com/1/classes/_User/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        }
    }
}]);