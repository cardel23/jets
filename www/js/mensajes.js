angular.module('starter.mensajes',[])

.factory('ChatService', function () {
	var mensajes = [{
		id:0,
		remitente:'Juan Perez',
		cuerpo:'Hola, bienvenido al sistema. Yo seré su técnico especializado para cualquier pregunta que tenga respecto de la aplicación. Un saludo.',
		orientacion:'left'
	}];
	return{
		all:function(){
			return mensajes;
		},
		get:function(id){
			return mensajes[id];
		}
	}
})