angular.module('starter.vuelo',[])

.value('PARSE_CREDENTIALS',{
    APP_ID: 'KerHLc4m0puEnR7su5AY60o1BK0fbdom4ASZtKIR',
    REST_API_KEY:'r1asVx1DmbYLlRvSEQytHhR71vfb8Jsgse6KyAze'
})

.factory('VueloService', ['$http', '$q','$timeout','PARSE_CREDENTIALS',function($http,$q,$timeout,PARSE_CREDENTIALS){
    // var Vuelo = Parse.Object.extend("Vuelo");
    var vuelo = {};
    var vuelos = [];
    var legs;
    var pax;
    var legArray = [];
    var asientos = [{num:1,car:"pasajero"},
        {num:2,car:"pasajeros"},
        {num:3,car:"pasajeros"},{num:4,car:"pasajeros"},{num:5,car:"pasajeros"},{num:6,car:"pasajeros"},
        {num:7,car:"pasajeros"},{num:8,car:"pasajeros"},{num:9,car:"pasajeros"},{num:10,car:"pasajeros"},
        {num:11,car:"pasajeros"},{num:12,car:"pasajeros"},{num:13,car:"pasajeros"},{num:14,car:"pasajeros"},
        {num:15,car:"pasajeros"},{num:16,car:"pasajeros"},{num:17,car:"pasajeros"},{num:18,car:"pasajeros"},
        {num:19,car:"pasajeros"}];
    return {
        getArrayVuelos:function() {
            console.log("Indice", vuelos.indexOf(vuelo))
            if(vuelos.indexOf(vuelo)==-1){
                vuelos.push(vuelo);
            }
            return vuelos;
        },
        getAsientos:function(){
            return asientos;
        },
        getNuevoVuelo:function(){
            return new Vuelo();
        },
        getVuelo:function(){
            return vuelo;
// =======
//             if(vuelo===undefined){
//                 vuelo = new Vuelo();
//                 console.log(angular.toJson(vuelo));
//             };
//             return vuelo;
// >>>>>>> 7666a2f94ca0358c83b9111e0fb7ae73f1b5b301
        },
        getSalida:function(){
            return salida;
        },
        getPax:function(){
            return pax;
        },
        getAllLegs:function(){
            if(!legs){
                legs = $http.get('https://api.parse.com/1/classes/Legs',{
                    headers:{
                        'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                        'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    }
                });
            }
            return legs;            
        },
        getLeg:function(id){
            vuelo = $http.get('https://api.parse.com/1/classes/Legs/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
            return vuelo;
        },
        createLeg:function(data){
            return $http.post('https://api.parse.com/1/classes/Legs',data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        editLeg:function(id,data){
            return $http.put('https://api.parse.com/1/classes/Legs/'+id,data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        deleteLeg:function(id){
            return $http.delete('https://api.parse.com/1/classes/Legs/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        getLegArray:function(){
            // console.log(angular.toJson(legArray));
            if(legArray.indexOf(vuelo)==-1){
                legArray.push(vuelo);
            }
            return legArray;
        }
    }
}]);