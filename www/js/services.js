angular.module('starter.services',['starter.controllers'])

.factory('MenuService', function() {

  var menu = [
  {
    id:0,
    titulo:'Buscar Charters',
    descripcion:'',
    icono:'ion-search'
  },
  {
    id:1,
    titulo:'Escalas vacias',
    descripcion:'',
    icono:'ion-plane'
  },
  {
    id:2,
    titulo:'Mi perfil',
    descripcion:'',
    icono:'ion-briefcase'
  },
  {
    id:3,
    titulo:'Mensajes',
    descripcion:'',
    icono:'ion-chatboxes'
  }];

  return {
    all: function() {
      return menu;
    },
    get: function(itemId) {
      // Simple index lookup
      return menu[itemId];
    }
  }
});


