angular.module('starter.controllers', [])

.factory('ControllerService',['AvionService','LocacionService', 'UsuarioService',
	function(AvionService,LocacionService, UsuarioService){
	var usuario = {};
	var pax;
	var reserva = {};	
	var currentVuelo = {};
	return{
		setVuelo:function (item){

			AvionService.get(item.avion.objectId).then(function(avion){
				item.avion = avion.data;
				// console.log('Check avion', angular.toJson(item.avion));
			});

			LocacionService.get(item.salida.objectId).then(function(salida){
				item.salida = salida.data;
			});

			LocacionService.get(item.destino.objectId).then(function(destino){
				item.destino = destino.data;
			});

			
			// console.log('Check currentVuelo const', angular.toJson(currentVuelo));
		},
		getCurrentVuelo:function(){
			// console.log('Check currentVuelo', angular.toJson(currentVuelo));
			return currentVuelo;
		},
		setPax: function(_pax){
			pax = _pax;
		},
		getPax: function(){
			return pax;
		},
		getUsuario: function(){
			UsuarioService.getUser().then(function(data){
				usuario = data.data;
			});
			return usuario;
		},
		getReserva:function(){
			return reserva;
		},
		setUser:function(item){
            console.log('Constructor');
            // console.log(angular.toJson(item));
            usuario = {
                "objectId": item.objectId,
                "email": item.email,
                "firstName": item.firstName,
                "lastName": item.lastName,
                "emailVerified": item.emailVerified,
                "ACL": item.ACL,
                "password": item.password,
                "authData": item.authData,
                "createdAt": item.createdAt,
                "phone": item.phone,
                "username": item.username,
                "membership": item.membership
            };
        },
        getUser:function(){

        	console.log('Check usuario', angular.toJson(usuario));
        	return usuario;
        }
	}
}])


.controller('MenuCtrl', function($scope, MenuService) {

	$scope.menus = MenuService.all();
	$scope.getMenu = function (index) {
		var menu = MenuService.get(index);
		return menu;
	};
  
})

.controller('MenuDetalleCtrl', function($scope, $stateParams, MenuService, MenuCtrl) {
	
	$scope.menu = MenuService.get($stateParams.menuId);

})

.controller("ChatCtrl", function($scope, ChatService, UsuarioService) {
	$scope.mensajes = ChatService.all();
	$scope.mensaje;
	
	function mensajeNuevo(){
		$scope.mensaje.id = $scope.mensajes.length+1;
		$scope.mensaje.remitente = UsuarioService.get('h7D2wy5lIs');
		$scope.mensaje.orientacion = 'right';
		$scope.mensajes.push($scope.mensaje);
		console.log($scope.mensajes);
	}

})

/*
Nota: lo que va en el arreglo del controlador debe ir en el mismo orden dentro de la funcion
	Ejemplo:
	.controller('nombreCtrl',[arg1,arg2,function(arg1,arg2){}])
*/

.controller('LocacionCtrl',['$scope','LocacionService',
	'$state','VueloService','$ionicPopup', 
	// '$ionicPopover',
	function($scope,LocacionService,$state,VueloService,
		$ionicPopup
		// ,$ionicPopover
		){

    LocacionService.getAll().success(function(data){
    	$scope.locaciones = data.results;
    });

    function selectSalida(data){
		LocacionService.setSalida(data);
	};

	function selectDestino(data){
		LocacionService.setDestino(data);
	};

    $scope.navBackSalida=function(obj) {
    	// console.log(obj);
    	selectSalida(obj);
    	$state.go('tab.menu0');
    }

    $scope.navBackDestino=function(obj) {
    	// console.log(obj);
    	selectDestino(obj);
    	$state.go('tab.menu0');
    }

    $scope.listaSalidas=function(){
        // Todo.create({content:$scope.todo.content}).success(function(data){
            $state.go('tab.listaSalidas');
        // });
    };

    $scope.listaDestinos=function(){
        // Todo.create({content:$scope.todo.content}).success(function(data){
            $state.go('tab.listaDestinos');
        // });
    };

    $scope.openDatePicker = function() {
	    $scope.tmp = {};
	    $scope.tmp.newDate = {};
	    
	    var birthDatePopup = $ionicPopup.show({
	     template: '<datetimepicker class="datepicker" ng-model="tmp.newDate">\
	     </datetimepicker>',
	     title: "Fecha de vuelo",
	     scope: $scope,
	     buttons: [
	       { text: 'Cancel' },
	       {
	        text: '<b>Save</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	        	$scope.vuelo.fechaVuelo = $scope.tmp.newDate;
	        }
	       }
	    ]
		});
	}
    $scope.showSalidas = function(){
    	var salidaPopup = $ionicPopup.show({
		    template: '<div style="height:100%;">\
				<div class="list list-inset">\
				  <label class="item item-input">\
				    <i class="icon ion-search placeholder-icon"></i>\
				    <input type="text" ng-model="filtro" placeholder="Buscar">\
				  </label>\
				</div>\
			</div>\
			<div>\
			<ion-list ng-show="filtro">\
			<div class="list list-inset">\
				<ion-item ng-click="navBack(item)" ng-repeat="item in locaciones | filter:filtro" >\
					<h4 ng-show="item.iata">{{item.nombre}}, {{item.iata}}</h4>\
					<h4 ng-show="!item.iata">{{item.nombre}} {{item.iata}}</h4>\
					<h4> {{item.ciudad}}, {{item.pais}}</h4>\
				</ion-item>\
				</div>\
			</div>',
		    title: '',
		    scope: $scope,
		    buttons: [
		    { text: 'Cancel' }
		    ]
		});
    };
}])

.controller('UsuarioCtrl',['$scope','UsuarioService','ControllerService',function($scope, UsuarioService,ControllerService) {

	var login = false;
	//success para varios elementos
	UsuarioService.getAll().success(function(data){
        $scope.items=data.results;
    });

	//then para un solo elemento
    UsuarioService.get("h7D2wy5lIs").then(function(data){
    	$scope.item = data.data;
    	if(ControllerService.getUser().objectId!=data.data.objectId){
    		console.log('Check');
    		ControllerService.setUser(data.data);
    	}
    });
	
	$scope.guardarDatosUsuario=function(){
        UsuarioService.edit($scope.item.objectId,
        	{firstName: $scope.item.firstName,
        	 lastName: $scope.item.lastName,
        	 phone: $scope.item.phone,
        	 email: $scope.item.email}).success(function(data){
            console.log('Exito');
        }).error(function(err){
        	console.log("Error: "+angular.toJson(err));
        });
    };

	$scope.onItemDelete = function (item) {
		UsuarioService.delete(item.objectId);
		$scope.items.splice($scope.items.indexOf(item),1);
	};
}])

/**
	No se por que no funciona
*/
// .controller('LocacionCtrl', ['$scope','LocacionService', '$state', function($scope, $stateParams, LocacionService, $state){
// 	$scope.listaDestinos = function(data){
// 		$state.go('tab.listaDestinos');
// 	};
// }])

.controller('LegCtrl', ['$scope', 
	'$http', '$timeout', 'VueloService', 
	'$ionicLoading','ControllerService',
	function($scope, $http, $timeout, VueloService, $ionicLoading, ControllerService){

		VueloService.getAllLegs().success(function (data) {

			$ionicLoading.show({
				scope: $scope,
				content: "Cargando ofertas",
				animation: "fade-in",
				showBackdrop: false,
				maxWidth: 200,
				showDelay: 500
			});

			$timeout(function(){
				
				angular.forEach(data.results, function (item) {
					ControllerService.setVuelo(item);
				});

				$scope.vuelos = data.results;
				$ionicLoading.hide();
			}, 2000);
		});
}])

.controller('DetalleLegCtrl',['$scope','$stateParams','VueloService','ControllerService','AvionService','LocacionService', 'UsuarioService',
	function($scope,$stateParams, VueloService,ControllerService,AvionService,LocacionService, UsuarioService){
		
		//Para llamada sincrona, se debe anidar las promises con then(then());
		VueloService.getLeg($stateParams.idEscala).then(function(data){
			AvionService.get(data.data.avion.objectId).then(function(avion){
				data.data.avion = avion.data;
				LocacionService.get(data.data.salida.objectId).then(function(salida){
					data.data.salida = salida.data;
					LocacionService.get(data.data.destino.objectId).then(function(destino){
						data.data.destino = destino.data;
						console.log('Check data', angular.toJson(data));
						$scope.reserva = ControllerService.getReserva();
						$scope.vuelo = data.data;
						$scope.reserva.vuelo = $scope.vuelo;
						$scope.asientos = VueloService.getAsientos();
						$scope.pax = VueloService.getPax();
						$scope.usuario = ControllerService.getUser();
						console.log('Check reserva', angular.toJson($scope.reserva.vuelo));
						if(!$scope.usuario.membership){
							$scope.reserva.tarifa = $scope.vuelo.precioNormal;
						}
						if($scope.usuario.membership){
							$scope.reserva.tarifa = $scope.vuelo.precioMiembro;
						}
						$scope.reserva.vuelo = $scope.vuelo;
						$scope.reserva.cliente = $scope.usuario;
						console.log('Check scope vuelo', angular.toJson($scope.reserva));
					});
				});
			});
		});
		$scope.pax = ControllerService.getPax();
	}]
)

.controller('FacturaLegCtrl',function($scope, ControllerService){
	$scope.reserva = ControllerService.getReserva();
	var _reserva = $scope.reserva;
	var _tarifa = _reserva.tarifa;
	var _cantidad = _reserva.pax;
	_cantidad = parseInt(_cantidad, 10);
	var _subTotal = _tarifa*_cantidad;
	var _total = _subTotal*1.15;
	_reserva.subTotal = _subTotal;
	_reserva.total = _total;
	console.log(angular.toJson($scope.reserva.pax));
})

.controller('GuardarLegCtrl', ['$scope', '$ionicPopup', 'ControllerService', 'ReservaService',function($scope, $ionicPopup, ControllerService, ReservaService){
	var _reserva = ControllerService.getReserva();
	var _cliente = _reserva.cliente.objectId;
	var _vuelo = _reserva.vuelo.objectId;
	console.log(angular.toJson(_reserva.pax));
	_reserva.pax = parseInt(_reserva.pax,10);
	console.log(angular.toJson(_reserva.pax));
	_reserva.status = false;
	_reserva.confirmacion = false;
	_reserva.prima = _reserva.total*0.10;

	_reserva.cliente = {
		"__type":"Pointer",
		"className": "_User",
		"objectId": _cliente
	};

	_reserva.vuelo = {
		"__type":"Pointer",
		"className": "Legs",
		"objectId": _vuelo
	};

	$scope.tarjeta = {};


	function showAlert() {
	   var alertPopup = $ionicPopup.alert({
	     title: 'Don\'t eat that!',
	     template: "<style>.popup { width:100%!important; }</style><p>It might taste good<p/>"
	   });
	   alertPopup.then(function(res) {
	     console.log('Thank you for not eating my delicious ice cream cone');
	   });
	 };

	 function showConfirm(item){
	 	var popup = $ionicPopup.confirm({
		    title: 'Confirmar tarjeta',
		    template: "<style>.popup { width:100%!important; }</style><p>Desea comprar el vuelo<p/>",
		    buttons:[{
		    	text:'Cancelar'
		    },
		    {
		    	text:'Aceptar',
		    	onTap:function(e){
		    		reservarVuelo();
		    	}
		    }]
	   	});
	 //    popup.then(function(res) {
		//     if(res) {
		//     	ReservaService.create(item);
		//     } else {
		//     	console.log('You are not sure');
		//     }
		// });
	 };
	
	$scope.abrirConfirmacion = function($ionicPopup){
		console.log(angular.toJson($scope.tarjeta));
		if($scope.tarjeta.mes 
			&& $scope.tarjeta.year 
			&& $scope.tarjeta.cvv)
		{
			showConfirm();
		}
		else{
			showAlert();
		}
		
	};

	function reservarVuelo(){
		_reserva.tarjeta = $scope.tarjeta;
		console.log(angular.toJson(_reserva));
		ReservaService.create(_reserva).success(function(data){
			console.log('Exito', angular.toJson(data));
		}).error(function(err){
			console.log('Error', angular.toJson(err));
		});
	};
}])

.controller('CharterCtrl', ['$scope', '$stateParams', 
	'$http', '$ionicPopup', '$timeout', 'VueloService', 
	'AvionService', 'LocacionService', 
	function($scope, $stateParams, $http, $ionicPopup, 
		$timeout, VueloService, AvionService, LocacionService) {


	$scope.avion = AvionService.get($stateParams.jetId);

	LocacionService.getAll().success(function(data){
        $scope.destinos = data.results; 
    });

	$scope.salida = LocacionService.getSalida();
	$scope.destino = LocacionService.getDestino();

	if($scope.vuelo == undefined){
		$scope.vuelo = getVuelo();	
	};
	
	$scope.asientos =  VueloService.getAsientos();
	
	if($scope.salida){
		$scope.vuelo.salida = $scope.salida;
	};

	if($scope.destino){
		$scope.vuelo.destino = $scope.destino;
	};

	$scope.legs = VueloService.getLegArray();
	console.log($scope.legs.indexOf($scope.vuelo));
	// if(!$scope.legs.indexOf($scope.vuelo)>-1){
	// 	$scope.legs.push($scope.vuelo);
	// }
	
	$scope.legs = VueloService.getArrayVuelos();
	console.log(angular.toJson($scope.legs));

	//Inicializar el vuelo con objeto vacio/obtenerlo si ya existe

	$scope.addRetorno = function(vuelo){
		// var retorno = {
		// 	"salida":vuelo.destino,
		// 	"destino":vuelo.salida,
		// 	"fechaVuelo":undefined,
		// 	"pax":undefined,

		// };
		var retorno = new VueloService.getVuelo();
		$scope.legs.push(retorno);
	};

	$scope.addLeg = function(vuelo){
		var ruta = {
			salida:vuelo.destino,
			"destino":undefined,
			"fechaVuelo":undefined,
			"pax":undefined,

		};
		$scope.legs.push(ruta);
	}

	function getVuelo(){
		var item = VueloService.getVuelo();
		return item;
	};

	$scope.newVuelo = function () {
		return VueloService.getNuevoVuelo();	
	};

	$scope.openDatePicker = function() {
	    $scope.tmp = {};
	    $scope.tmp.newDate = $scope.vuelo.fechaVuelo;
	    
	    var birthDatePopup = $ionicPopup.show({
	     template: '<datetimepicker class="datepicker" ng-model="tmp.newDate"></datetimepicker>',
	     title: "Fecha de vuelo",
	     scope: $scope,
	     buttons: [
	       { text: 'Cancel' },
	       {
	        text: '<b>Save</b>',
	        type: 'button-positive',
	        onTap: function(e) {
	        	$scope.vuelo.fechaVuelo = $scope.tmp.newDate;
	        }
	       }
	    ]
		});
	}

	$scope.showSalidas = function(){
		var salidaPopup = $ionicPopup.show({

		});
	}

	 // A confirm dialog
	 $scope.showConfirm = function() {
	   var confirmPopup = $ionicPopup.confirm({
	     title: 'Confirmar tarjeta',
	     template: '¿Desea comprar el vuelo?',
	     buttons:[{
	     	text:'Cancelar'
	     },
	     {
	     	text:'Aceptar'
	     }]
	   });
	   confirmPopup.then(function(res) {
	     if(res) {
	       alert('Aceptar');
	     } else {
	       console.log('You are not sure');
	     }
	   });
	 };

	 // An alert dialog
	 $scope.showAlert = function($ionicPopup) {
	   var alertPopup = $ionicPopup.alert({
	     title: 'Don\'t eat that!',
	     template: 'It might taste good'
	   });
	   alertPopup.then(function(res) {
	     console.log('Thank you for not eating my delicious ice cream cone');
	   });
	 };

}]);


