 // Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services','starter.vuelo','starter.avion',
  'starter.usuario','starter.mensajes','starter.destinos','starter.reserva','starter.directives',
  'ui.bootstrap.datetimepicker'])


.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tab.menu', {
      url: '/menu',
      views: {
        'index-tab': {
          templateUrl: 'templates/index-tab.html',
          controller: 'MenuCtrl'
        }
      }
    })

    .state('tab.listaSalidas',{
      url: '/salidas',
      views: {
        'index-tab':{
          templateUrl: 'templates/listaSalidas.html',
          controller: 'LocacionCtrl'
        }
      }
    })

    .state('tab.listaDestinos',{
      url: '/destinos',
      views: {
        'index-tab':{
          templateUrl: 'templates/listaDestinos.html',
          controller: 'LocacionCtrl'
        }
      }
    })

    .state('tab.menu0', {
      url: '/menu/0',
      views: {
        'index-tab': {
          templateUrl: 'templates/buscarCharter.html',
          controller: 'CharterCtrl'
        }
      }
    })

    .state('tab.lista', {
      url: '/menu/0/listaDisponibles',
      views: {
        'index-tab': {
          templateUrl: 'templates/listaDisponibles.html',
          controller: 'CharterCtrl'
        }
      }
    })

    .state('tab.programaCharter',{
      url: '/menu/0/listaDisponibles/:jetId',
      views: {
        'index-tab':{
          templateUrl: 'templates/programarVuelo.html',
          controller: 'CharterCtrl'
        }
      }
    })

    .state('tab.facturaCharter',{
      url: '/menu/0/listaDisponibles/:jetId/factura',
      views: {
        'index-tab':{
          templateUrl: 'templates/facturaCharter.html',
          controller: 'CharterCtrl'
        }
      }
    })

    .state('tab.confirmarCharter',{
      url: '/menu/0/listaDisponibles/:jetId/confirmar',
      views: {
        'index-tab': {
          templateUrl: 'templates/confirmaEscala.html',
          controller: 'CharterCtrl'
        }
      }
    })

    .state('tab.escalas', {
      url: '/menu/1',
      views: {
        'index-tab': {
          templateUrl: 'templates/escalasVacias.html',
          controller: 'LegCtrl'
        }
      }
    })

    .state('tab.reservaEscala',{
      url: '/menu/1/reservaEscala/:idEscala',
      views: {
        'index-tab':{
          templateUrl: 'templates/reservaLeg.html',
          controller: 'DetalleLegCtrl'
        }
      }
    })

    .state('tab.facturaEscala',{
      // url: '/menu/1/factura',
      url: '/menu/1/reservaEscala/:idEscala/factura',
      views: {
        'index-tab':{
          templateUrl: 'templates/factura.html',
          controller: 'FacturaLegCtrl'
        }
      }
    })

    .state('tab.confirmaEscala',{
      // url: '/menu/1/factura',
      url: '/menu/1/reservaEscala/:idEscala/confirmar',
      views: {
        'index-tab':{
          templateUrl: 'templates/confirmaEscala.html',
          controller: 'GuardarLegCtrl'
        }
      }
    })

    .state('tab.perfil', {
      url: '/menu/2',
      views: {
        'index-tab': {
          templateUrl: 'templates/datosCuenta.html',
          controller: 'UsuarioCtrl'
        }
      }
    })

    .state('tab.editarPerfil',{
      url: '/editarPerfil',
      views:{
        'index-tab':{
          templateUrl:'templates/editarCuenta.html',
          controller: 'UsuarioCtrl'
        }
      }
    })

    .state('tab.mensajes', {
      url: '/menu/3',
      views: {
        'index-tab': {
          templateUrl: 'templates/mensajes.html',
          controller: 'ChatCtrl'
        }
      }
    })

    .state('tab.nuevoMensaje',{
      url: '/nuevoMensaje',
      views:{
        'index-tab':{
          templateUrl: 'templates/nuevoMensaje.html',
          controller: 'ChatCtrl'
        }
      }
    })

    .state('tab.cuenta', {
      url: '/cuenta',
      views: {
        'cuenta-tab': {
          templateUrl: 'templates/cuenta.html'
        }
      }
    })

    .state('tab.buscar', {
      url: '/buscar',
      views: {
        'index-tab': {
          templateUrl: 'templates/buscar.html'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/menu');

})

.run(function($state){
    $state.go('tab.menu')
});

