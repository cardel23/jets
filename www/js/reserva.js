angular.module('starter.reserva',[])

.value('PARSE_CREDENTIALS',{
    APP_ID: 'KerHLc4m0puEnR7su5AY60o1BK0fbdom4ASZtKIR',
    REST_API_KEY:'r1asVx1DmbYLlRvSEQytHhR71vfb8Jsgse6KyAze'
})

.factory('ReservaService', ['$http','PARSE_CREDENTIALS',function($http, PARSE_CREDENTIALS){
	var reservas;
	var reserva;
	return{
		getAll:function(){
            reservas = $http.get('https://api.parse.com/1/classes/Reserva',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
            return reservas;
        },
        get:function(id){
            return $http.get('https://api.parse.com/1/classes/Reserva/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
        },
        create:function(data){
            return $http.post('https://api.parse.com/1/classes/Reserva',data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        edit:function(id,data){
            return $http.put('https://api.parse.com/1/classes/Reserva/'+id,data,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        },
        delete:function(id){
            return $http.delete('https://api.parse.com/1/classes/Reserva/'+id,{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key':PARSE_CREDENTIALS.REST_API_KEY,
                    'Content-Type':'application/json'
                }
            });
        }
	}
}]);